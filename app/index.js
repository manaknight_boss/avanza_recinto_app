import React, { Component } from "react";

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import RNExitApp from 'react-native-exit-app';
import LoginScreen from './screens/Login';

import RegisterScreen from './screens/Register';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import PerssonelScreen from './screens/Perssonel';
import PerssonelViewScreen from './screens/PerssonelView';
import DashBoardScreen from './screens/DashBoard';
import SplashScreen from './screens/Splash';
import CustomNavigationDrawer from "./components/DrawerMenu";
import HelpScreen from "./screens/HelpScreen";

const AuthNavigator = createStackNavigator({

  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      headerShown: false
    }
  }
});
const PerssonelNavigator = createStackNavigator({

  PerssonelScreen: {
    screen: PerssonelScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  PerssonelViewScreen: {
    screen: PerssonelViewScreen,
    navigationOptions: {
      headerShown: false
    }
  }
});

const AppDrawerNavigator = createDrawerNavigator(
  {
    Tablero: DashBoardScreen,
    Personal: PerssonelNavigator,
    Asistencia : HelpScreen,
    Salir: () => {
      RNExitApp.exitApp();
      return (null);
    },
  },
  {
    contentComponent: CustomNavigationDrawer,
    initialRouteName: "Tablero",
  }
);
const Application = createAppContainer(createSwitchNavigator(
  {

    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    },
    Auth: AuthNavigator,
    Home: AppDrawerNavigator
  },
  {
    initialRouteName: 'SplashScreen',
    backBehavior: 'App'
  }
));


export default Application;