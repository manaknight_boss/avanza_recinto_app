import React, { Component } from 'react';
import { Image, View } from 'react-native';
import styles from "./style"

export default class SplashScreen extends Component {
    
    componentDidMount() {
        
        setTimeout(() => {
            this.props.navigation.navigate('Auth');
        }, 2000);
        
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../asserts/images/logo.jpg')} style={{width:"100%", height: "100%"}}/>
            </View>
        );
    }
}