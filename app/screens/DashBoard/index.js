import React from "react";

import { Text, FlatList, View,SafeAreaView , Image, Linking, Platform, TouchableOpacity, Alert,Dimensions  } from "react-native"
import styles from "./style"
import { Col, Row, Grid } from "react-native-easy-grid";
import strings from "../../asserts/strings";
import AppLoader from "../../components/ActivityIndicator";
import AsyncStorage from "@react-native-community/async-storage";


export default class PerssonelScreen extends React.Component {



  state = {
    id: '',
    loading: true,
    recinto: '',
    recintoName: '',
    collegio: '',
    coordinatorName: '',
    coordinatorGovernmentId: '',
    usersList: [],
    personel:'',
    assigned:'',
    present:'',
    createdAt:'',
    updatedAt:'',
    subscribed:0,
    voted:0,
    notVoted:0
  }


  constructor(props) {
    super(props);
  
  }

  componentDidMount() {
    AsyncStorage.getItem('profile').then((value) => {
      var profile = JSON.parse(value)
      this.setState({ recinto: profile.recinto });
      this.setState({ id: profile.id });
      this.setState({ recintoName: profile.recinto_name});
      this.setState({ collegio: profile.collegio});
      this.setState({ coordinatorName: profile.coordinator_name });
      this.setState({ coordinatorGovernmentId: profile.coordinator_government_id});
      this.setState({ personel: profile.personel});
      this.setState({ assigned: profile.assigned});
      this.setState({ present: profile.present});
      this.setState({ createdAt: profile.created_at});
      this.setState({ updatedAt: profile.updated_at});
      this.getDashboard();
    })

    

    
  }
  getDashboard = () =>{
    var baseUrl = strings.baseUrlAndroid;
    if (Platform.OS === 'ios') {
      baseUrl = strings.baseUrl
    }
    const url = baseUrl + 'v1/api/recinto/dashboard/' + this.state.recinto;
    console.log(url);
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        if (response.status == 200) {
          return response.text()
        } else {
          return "" + response.status
        }
      }).then((text) => {
        this.setState({ loading: false });
        console.log(text);
        const res = JSON.parse(text);
        this.setState({ usersList: res.data });
        this.setState({ subscribed: res.total });
        this.setState({ voted: res.voted });
        this.setState({ notVoted: res.not_voted });
      }).catch((error) => {
        console.error(error);
      });
  }

  dialCall = (number) => {
    console.log("number", number);
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else { phoneNumber = `telprompt:${number}`; }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  _rowRenderer(type, data) {
    
    <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]}>
    <Col>
      <Text style={styles.cell}>{data.first_name}</Text>
    </Col>
    <Col>
      <Text style={styles.cell}>{data.last_name}</Text>
    </Col>
    <Col><View style={{ flex: 1, flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
      {data.phone_1 != "" ?
        <TouchableOpacity onPress={() => this.dialCall(data.phone_1)}><Image source={require('../../asserts/images/smartphone.png')}
          style={{ width: 20, height: 30 }}
        /></TouchableOpacity> : null}
      <Text>{' '}</Text>
      {data.phone_2 != "" ?
        <TouchableOpacity onPress={() => this.dialCall(data.phone_2)}><Image source={require('../../asserts/images/smartphone.png')}
          style={{ width: 20, height: 30 }}
        /></TouchableOpacity> : null}
    </View>
    </Col>
  </Row>
            
}

  renderRow(data, index) {
    if(data.staff_lead_name === '')
    {
      data.staff_lead_name="-";
    }
    return (
      <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]} key={data.phone_1}>
        <Col>
          <Text style={styles.cell}>{data.college}</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>{data.percentage}</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>{data.staff_lead_name}</Text>
        </Col>
        <Col><View style={{ flex: 1, flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
          {data.phone_1 != "" ?
            <TouchableOpacity onPress={() => this.dialCall(data.phone_1)}><Image source={require('../../asserts/images/smartphone.png')}
              style={{ width: 20, height: 30 }}
            /></TouchableOpacity> : null}
          <Text>{' '}</Text>
          {data.phone_2 != "" ?
            <TouchableOpacity onPress={() => this.dialCall(data.phone_2)}><Image source={require('../../asserts/images/smartphone.png')}
              style={{ width: 20, height: 30 }}
            /></TouchableOpacity> : null}
        </View>
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <SafeAreaView  style={styles.container}>
        {this.state.loading ? <AppLoader /> : 
          <Grid>
           <Row style={styles.header} size={2}>
             <Row  size={1}>
             <TouchableOpacity  style={styles.closeIcon}  onPress={()=>{
                this.props.navigation.openDrawer();
                // this.props.navigation.navigate('DrawerOpen', {userName: "hello"});
              }}>
          <Image source={require("../../asserts/images/list.png")} />
          </TouchableOpacity>           
            </Row>
             <Row style={styles.nameBlock}  size={2}>
               <Text style={[styles.textCenter, styles.topHeading]}>{this.state.recinto}{' '}{this.state.recintoName}</Text>
             </Row>
           </Row>
           <Row size={2} style={{ backgroundColor: 'orange' }}>
             <Col>
               <Row style={styles.flexEnd}>
                 <Text style={[styles.textCenter]}>{strings.subscribed}</Text>
               </Row>
               <Row>
                 <Text style={[styles.textCenter , styles.topHeading]}>{this.state.subscribed}</Text>
               </Row>
             </Col>
             <Col>
               <Row style={styles.flexEnd}>
                 <Text style={[styles.textCenter]}>{strings.voted}</Text>
               </Row>
               <Row>
                 <Text style={[styles.textCenter, styles.topHeading]}>{this.state.voted}</Text>
               </Row>
             </Col>
             <Col>
               <Row style={styles.flexEnd}>
                 <Text style={[styles.textCenter]}>{strings.notVoted}</Text>
               </Row>
               <Row>
                 <Text style={[styles.textCenter, styles.topHeading]}>{this.state.notVoted}</Text>
               </Row>
             </Col>
           </Row>
 
           <Row style={styles.heading} size={1}>
             <Text style={[styles.textCenter,styles.headingText]}>{strings.dashboardHead}</Text>
           </Row>
           <Col size={8} >
             <Row size={1} style={styles.tableHeader}>
               <Col>
                 <Text style={styles.tableHeading}>
                {strings.college}
                </Text>
               </Col>
               <Col>
                 <Text style={styles.tableHeading}>{strings.pct}</Text>
                
               </Col>
               <Col>
                 <Text  style={styles.tableHeading}>{strings.staffLead}</Text>
               </Col>
               <Col>
                 <Text  style={styles.tableHeading}>{strings.action}</Text>
               </Col>
             </Row>
 
             <Col size={8}>
               <FlatList
               data={this.state.usersList}
               renderItem={({ item, index }) => this.renderRow(item, index)}
               keyExtractor={(item, index) => {
                return index.toString();
              }}
               removeClippedSubviews={true}
               maxToRenderPerBatch={this.state.usersList.length}
             />

             </Col>
           </Col>
         </Grid>
       
      }
      </SafeAreaView >

    );
  }
}
