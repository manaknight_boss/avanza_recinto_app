import React from "react";
import { button, View, Text, TouchableOpacity, Keyboard, Button, TextInput, Image, SafeAreaView, Platform } from "react-native"
import commonStyles from "../../asserts/style"
import styles from "./style"
import strings from "../../asserts/strings";
import AppLoaderLessOpacity from "../../components/ActivityIndicatorLessOpacity";
import { Dialog } from 'react-native-simple-dialogs';
import colors from "../../asserts/colors";
import AsyncStorage from "@react-native-community/async-storage";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class HomeScreen extends React.Component {

    state = {
        id: '',
        password: '',
        loading: false,
        modalVisible: false,
        error: ''
    }

    showErrorModal(error) {
        this.setState({ modalVisible: true });
        this.setState({ error: error });
    }

    handleId = (text) => {
        this.setState({ id: text })
    }
    handlePassword = (passwordText) => {
        this.setState({ password: passwordText })
    }

    apiCall = (id, password) => {
        var baseUrl = strings.baseUrlAndroid;
        if (Platform.OS === 'ios') {
        baseUrl = strings.baseUrl
        }
        const url = baseUrl + 'v1/api/recinto/login'
        console.log("api", url);
        console.log("body", JSON.stringify({
            government_id: id,
            password: password
        }));
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                government_id: id,
                password: password
            }),
        }).then((response) => {
            if (response.status == 200) {
                return response.text()
            } else {
                return "" + response.status
            }

        })
            .then((text) => {

                console.log("response", text);
                if (text == 403) {
                    this.setState({ loading: false });
                    this.showErrorModal(strings.wrongIdOrPassword);
                    // this.setState({ error: strings.wrongEmailOrPassword });
                } else {

                    AsyncStorage.setItem('id', id);
                    this.getProfile();
                    // this.props.navigation.navigate("Home");
                    // this.props.navigation.replace("Home");
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getProfile() {
        AsyncStorage.getItem('id').then((value) => {      //for getting secret and opening screen accordingly on profile button click
            // this.setState({id:value})
            console.log(this.state.id)
            var baseUrl = strings.baseUrlAndroid;
            if (Platform.OS === 'ios') {
            baseUrl = strings.baseUrl
            }
            const url = baseUrl + 'v1/api/recinto/profile/' + value;
            console.log("Api == " + url);
            fetch(url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then((response) => {
                if (response.status == 200) {
                    return response.text()
                } else {
                    return "" + response.status
                }
            }).then((text) => {
                this.setState({ loading: false });
                console.log(text);
                const res = JSON.parse(text);
                AsyncStorage.setItem("profile", JSON.stringify(res.data));
                this.props.navigation.navigate("Home");
            }).catch((error) => {
                console.error(error);
            });
        })
    }

    login = (id, pass) => {
        if (id == '') {
            this.showErrorModal(strings.idRequired);
            // this.setState({ error: strings.emailRequired });
            return
        }
        if (pass == '') {
            this.showErrorModal(strings.passwordRequired);
            // this.setState({ error: strings.passwordRequired });
            return
        }

        Keyboard.dismiss();

        this.setState({ loading: true });
        this.apiCall(id, pass)

    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView contentContainerStyle={styles.containerInside}>
                <View >
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Image
                            source={require('../../asserts/images/account.png')}
                            style={styles.margin}
                        />
                    </View>

                    <Text style={[styles.margin, styles.textCenter]}>{strings.appName}</Text>
                    <View style={styles.margin}>
                        <Text style={{ padding: 5 }}>{strings.user}</Text>
                        <TextInput
                            style={styles.textField}
                            onChangeText={this.handleId}
                            autoCompleteType="off"
                            keyboardType="number-pad"
                            value={this.state.id}
                        />
                    </View>
                    <View style={styles.margin}>
                        <Text style={{ padding: 5 }}>{strings.password}</Text>
                        <TextInput style={styles.textField}
                            onChangeText={this.handlePassword}
                            autoCompleteType="off"
                            secureTextEntry={true}
                            value={this.state.password}
                        />
                    </View>

                    <TouchableOpacity style={[commonStyles.primaryButton, styles.margin]}
                        onPress={() => this.login(this.state.id, this.state.password)}>
                        <Text style={[commonStyles.primaryButtonText]}>{strings.login}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[ styles.margin]}
                        onPress={() => this.props.navigation.navigate('RegisterScreen')}>
                        <Text style={[commonStyles.primaryButtonText]}>{strings.register}</Text>
                    </TouchableOpacity>

                </View>
                </KeyboardAwareScrollView>
                <Dialog
                    title={strings.error}
                    color={colors.black}
                    titleStyle={commonStyles.bold}
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={() => { this.setState({ modalVisible: false }) }}
                    visible={this.state.modalVisible}
                >
                    <View>
                        <Text style={styles.dialogStyle}>{this.state.error}</Text>
                    </View>
                    <View style={styles.close}>
                        <Button onPress={() => {
                            this.setState({ modalVisible: false });
                        }}
                            title={strings.close}
                            color={colors.black} />

                    </View></Dialog>
                {this.state.loading ? <AppLoaderLessOpacity /> : null}
                
            </SafeAreaView >
        );
    }
}
