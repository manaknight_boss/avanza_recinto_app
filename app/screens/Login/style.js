import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';

var styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerInside: {
        flex: 1,
        flexDirection: 'column',
        alignContent: 'center',
        width: '80%',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    margin: {
        marginBottom: 14,
        marginTop: 14,
    },
    textCenter: {
        textAlign: 'center'
    },
    textField: {
        height: 40,
        borderColor: colors.grey,
        borderWidth: 1,
        color:colors.black
    },
    dialogStyle: {
        fontSize: 14,
        color: colors.black
    },
    close: {
        marginTop: 20,
        textAlign: 'center',
        justifyContent: 'center',
        marginStart: 17,
        marginEnd: 17,
        borderRadius: 10,
        backgroundColor: colors.button,
    }
})

export default styles