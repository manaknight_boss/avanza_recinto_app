import React, { Component } from 'react';
import {
  View,
  Linking,
  Platform,
  Alert
} from "react-native";
import strings from "../../asserts/strings";

export default class HelpScreen extends Component {

  componentDidMount() {

    let phoneNumber = strings.phoneNumber;
    if (Platform.OS === 'android') { phoneNumber = `tel:${phoneNumber}`; }
    else { phoneNumber = `telprompt:${phoneNumber}`; }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          Linking.openURL(phoneNumber);
          this.props.navigation.goBack();
        }
      })
      .catch(err => console.log(err));

  }

  render() {
    return (
      <View>
      </View>
    );
  }
}