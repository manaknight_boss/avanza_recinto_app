import React from "react";
import AsyncStorage from "@react-native-community/async-storage";

import { Text, FlatList, View, SafeAreaView, Image, Linking, Platform, TouchableOpacity, Alert, Dimensions } from "react-native"
import styles from "./style"
import CheckBox from 'react-native-check-box'

import { Col, Row, Grid } from "react-native-easy-grid";
import strings from "../../asserts/strings";
import AppLoader from "../../components/ActivityIndicator";


export default class PerssonelViewScreen extends React.Component {
  state = {
    id: '',
    objectId:'',
    loading: true,
    total: 0,
    voted: 0,
    notVoted: 0,
    list: [],
    perssonel: 0,
    present: 0,
    collegio:'',
    name: '',
    phone: ''
  }

  constructor(props) {
    super(props);

  }

  componentDidMount() {
    const recinto = this.props.navigation.state.params.recinto;
    const college = this.props.navigation.state.params.college;
    var baseUrl = strings.baseUrlAndroid;
      if (Platform.OS === 'ios') {
        baseUrl = strings.baseUrl
      }
      const url = baseUrl + 'v1/api/recinto/college/' + recinto + "/" + college;
    console.log("Api == " + url);
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status == 200) {
        return response.text()
      } else {
        return "" + response.status
      }
    }).then((text) => {
      this.setState({ loading: false });
      console.log(text);
      const res = JSON.parse(text);
      const result = res.data[0]
      var list = []
      list.push({name:result.staff_lead_name,position:strings.staffLeadPosition,isPresent:result.staff_lead_present,type:'staff_lead_present'})
      list.push({name:result.transmitter_name ,position:strings.transmitterPosition,isPresent:result.transmitter_present,type:'transmitter_present'})
      list.push({name:result.delegate_1_name,position:strings.delegateOnePosition,isPresent:result.delegate_1_present,type:'delegate_1_present'})
      list.push({name:result.delegate_2_name,position:strings.delegateTwoPosition,isPresent:result.delegate_2_present,type:'delegate_2_present'})
      list.push({name:result.runner_1_name,position:strings.runnerOnePosition,isPresent:result.runner_1_present,type:'runner_1_present'})
      list.push({name:result.runner_2_name,position:strings.runnerTwoPosition,isPresent:result.runner_2_present,type:'runner_2_present'})
      this.setState({ list: list, perssonel: res.personnel, present: res.present, collegio: res.data[0].collegio, name:  res.data[0].staff_lead_name, phone: res.phone,objectId:result.id});

    }).catch((error) => {
      console.error(error);
    });
  }

  updateAttencdance = (index) => {

    var list = this.state.list;
    var id = this.state.objectId;
    var stage = list[index].isPresent;
    var present = -1;
    if (stage == "0") {
      stage = "1";
      present = 1; 
    }
    else {
      stage = "0";
    }
    list[index].isPresent = stage;
    present = this.state.present+present
    this.setState({ list, present });
    var baseUrl = strings.baseUrlAndroid;
    if (Platform.OS === 'ios') {
      baseUrl = strings.baseUrl
    }
    const url = baseUrl + 'v1/api/recinto/update/' + id + "/"+list[index].type+"/" + stage;
    console.log("Api == " + url);
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).catch((error) => {
      console.error(error);
    });
  }
  dialCall = (number) => {
    console.log("number", number);
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else { phoneNumber = `telprompt:${number}`; }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          // Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  renderRow(data, index) {
 
    var isStaffPresent = false;
    if (data.isPresent == "1") {
      isStaffPresent = true;
    }
    return (
      <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]} key={data.phone_1}>
        <Col>
          <Text style={styles.cell}>{data.name}</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>{data.position}</Text>
        </Col>
        <Col style={[{ alignItems: 'center' }]}>
          {/* <CheckBox
            title=''
            value={isStaffPresent}
            onChange={() => {
              this.updateAttencdance(index);
            }}
          /> */}
          {/* onClick={()=>{
              this.setState({
                  isChecked:!this.state.isChecked
              })
            }} */}
        <CheckBox
            style={{flex: 1, padding: 10}}
            
            onClick={() => {
              this.updateAttencdance(index);
            }}
            isChecked={isStaffPresent}
            leftText={''}
        />

        </Col>

      </Row>
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loading ? <AppLoader /> :


          <Grid>

            <Row style={styles.header} size={2}>
              <Row size={1}>
                <TouchableOpacity style={styles.closeIcon} onPress={() => {
                  this.props.navigation.goBack();
                }}>
                  <Image source={require("../../asserts/images/back.png")} />
                </TouchableOpacity>
              </Row>
              <Row style={styles.nameBlock} size={2}>
                <Text style={[styles.textCenter, styles.topHeading]}>{this.state.collegio} {this.state.name}</Text>
              </Row>
              <Row style={styles.nameBlock} size={1}>
                <TouchableOpacity style={[styles.textCenter, styles.topHeading]} onPress={() => this.dialCall(this.state.phone)}>
                <Text  style={[styles.textCenter, styles.topHeading]}  >{this.state.phone}</Text>
                </TouchableOpacity>
              </Row>
            </Row>
            <Row size={2} style={{ backgroundColor: 'orange' }}>
              <Col>
                <Row style={styles.flexEnd}>
                  <Text style={[styles.textCenter]}>{strings.perssonel}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, styles.topHeading]}>{this.state.perssonel}</Text>
                </Row>
              </Col>
              <Col>
                <Row style={styles.flexEnd}>
                  <Text style={[styles.textCenter]}>{strings.present}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, styles.topHeading]}>{this.state.present}</Text>
                </Row>
              </Col>

            </Row>

            <Row style={styles.heading} size={1}>

              <Text style={[styles.textCenter, styles.headingText]}>{strings.viewPerssonelHead}</Text>

            </Row>

            <Col size={8} >
              <Row size={1} style={styles.tableHeader}>

                <Col>
                  <Text style={styles.tableHeading}>{strings.name}</Text>

                </Col>
                <Col>
                  <Text style={styles.tableHeading}>{strings.position}</Text>
                </Col>
                <Col>
                  <Text style={styles.tableHeading}>{strings.status}</Text>
                </Col>
              </Row>

              <Col size={8}>
                <FlatList
                  data={this.state.list}
                  renderItem={({ item, index }) => this.renderRow(item, index)}
                  keyExtractor={(item, index) => {
                    return index.toString();
                  }}
                  removeClippedSubviews={true}
                  maxToRenderPerBatch={this.state.list.length}
                />


              </Col>
            </Col>
          </Grid>

        }
      </SafeAreaView>
    );
  }
}
