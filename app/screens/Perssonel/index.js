import React from "react";
import AsyncStorage from "@react-native-community/async-storage";

import { Text, FlatList, SafeAreaView, Image, Linking, Platform, TouchableOpacity, Alert, Dimensions } from "react-native"
import styles from "./style"
import { Col, Row, Grid } from "react-native-easy-grid";
import strings from "../../asserts/strings";
import AppLoader from "../../components/ActivityIndicator";


export default class PerssonelScreen extends React.Component {



  state = {
    id: '',
    loading: true,
    firstName: '',
    lastName: '',
    list: [],
    totalCollege: 0,
    totalPerssonel: 0,
    totalPresent: 0,
    recinto: 0,
    recintoName: ''
  }


  constructor(props) {
    super(props);

  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("didFocus", () => {
    AsyncStorage.getItem('profile').then((value) => {      //for getting secret and opening screen accordingly on profile button click
      var profile = JSON.parse(value);
      this.setState({ recinto: profile.recinto, recintoName: profile.recinto_name })
      
      var baseUrl = strings.baseUrlAndroid;
      if (Platform.OS === 'ios') {
        baseUrl = strings.baseUrl
      }
      const url = baseUrl + 'v1/api/recinto/personnel/' + profile.recinto;
      console.log("Api == " + url);
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }
      }).then((response) => {
        if (response.status == 200) {
          return response.text()
        } else {
          return "" + response.status
        }
      }).then((text) => {
        this.setState({ loading: false });
        console.log(text);
        const res = JSON.parse(text);
        this.setState({ list: res.data });
        this.setState({ totalCollege: res.college });
        this.setState({ totalPerssonel: res.personal });
        this.setState({ totalPresent: res.present });
      }).catch((error) => {
        console.error(error);
      });
    })
  })
  }


  dialCall = (number) => {
    console.log("number", number);
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else { phoneNumber = `telprompt:${number}`; }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };


  renderRow(data, index) {
    return (
      <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]} key={data.phone_1}>
        <Col>
          <Text style={styles.cell}>{data.collegio}</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>6</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>{Number(data.staff_lead_present) + Number(data.transmitter_present) +Number(data.delegate_1_present)+Number(data.delegate_2_present)+Number(data.runner_1_present)+Number(data.runner_2_present)}</Text>
        </Col>
        <Col>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("PerssonelViewScreen", {
            recinto: this.state.recinto,
            college: data.collegio
          })}>
            <Text style={styles.cell}>
              {strings.view}
            </Text>
          </TouchableOpacity>
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loading ? <AppLoader /> :
          <Grid>
            <Row style={styles.header} size={2}>
              <Row size={1}>
                <TouchableOpacity style={styles.closeIcon} onPress={() => {
                  this.props.navigation.openDrawer();
                }}>
                  <Image source={require("../../asserts/images/list.png")} />
                </TouchableOpacity>
              </Row>
              <Row style={styles.nameBlock} size={2}>
                <Text style={[styles.textCenter, styles.topHeading]}>{this.state.recinto} {this.state.recintoName}</Text>
              </Row>
            </Row>
            <Row size={2} style={{ backgroundColor: 'orange' }}>
              <Col>
                <Row style={styles.flexEnd}>
                  <Text style={[styles.textCenter]}>{strings.college}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, styles.topHeading]}>{this.state.totalCollege}</Text>
                </Row>
              </Col>
              <Col>
                <Row style={styles.flexEnd}>
                  <Text style={[styles.textCenter]}>{strings.perssonel}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, styles.topHeading]}>{this.state.totalPerssonel}</Text>
                </Row>
              </Col>
              <Col>
                <Row style={styles.flexEnd}>
                  <Text style={[styles.textCenter]}>{strings.present}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, styles.topHeading]}>{this.state.totalPresent}</Text>
                </Row>
              </Col>
            </Row>
            <Row style={styles.heading} size={1}>
              <Text style={[styles.textCenter, styles.headingText]}>{strings.perssonelHead}</Text>
            </Row>
            <Col size={8} >
              <Row size={1} style={styles.tableHeader}>
                <Col>
                  <Text style={styles.tableHeading}>{strings.college}</Text>
                </Col>
                <Col>
                  <Text style={styles.tableHeading}>{strings.staff}</Text>
                </Col>
                <Col>
                  <Text style={styles.tableHeading}>{strings.present}</Text>
                </Col>
                <Col>
                  <Text style={styles.tableHeading}>{strings.action}</Text>
                </Col>
              </Row>

              <Col size={8}>
                <FlatList
                  data={this.state.list}
                  renderItem={({ item, index }) => this.renderRow(item, index)}
                  keyExtractor={(item, index) => {
                    return index.toString();
                  }}
                  removeClippedSubviews={true}
                  maxToRenderPerBatch={this.state.list.length}
                />
              </Col>
            </Col>
          </Grid>
        }
      </SafeAreaView>
    );
  }
}
