import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';

var styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerInside: {
        flex: 1,
        flexDirection: 'column',
        alignContent: 'center',
        width: '80%',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    margin: {
        marginBottom: 14,
        marginTop: 14,
        textAlign: 'center'
    },
    textCenter: {
        textAlign: 'center',
        width: '100%'
    },
    cell: {
        textAlign: 'center',
        fontSize: 14 
    },
    header: { 
        backgroundColor: 'orange', 
        alignItems: 'flex-start',
        flexDirection: 'column',
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        paddingTop: 20,
        width:"100%"
    },
    closeIcon:{
        marginLeft:15
    },
    heading:{ backgroundColor: '#000000', alignItems: 'center' },
    headingText: { color: 'white', fontSize: 16 },
    topHeading:{ fontSize: 17, color: 'white', fontWeight: 'bold' },
    flexEnd:{ alignItems: 'flex-end' },
    tableHeading:{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' },
    tableHeader:{ backgroundColor: '#d3d3d3', alignItems: 'center' },
    nameBlock:{flex: 1, justifyContent: "center", alignItems: "center" }
})

export default styles