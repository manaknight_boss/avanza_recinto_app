import React, {Component} from 'react';
import {View} from 'react-native';
import styles from './style'
import colors from '../../asserts/colors';

class StatusBarBackground extends Component{
  render(){
    return(
      <View style={[styles.statusBarBackground, {backgroundColor:colors.white}]}>
      </View>
    );
  }
}

export default StatusBarBackground