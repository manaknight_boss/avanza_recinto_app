import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';


var styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      color: colors.primary,
      alignItems: 'center',
      backgroundColor: colors.white,
      opacity: 1
    }
  })
   
  export default styles