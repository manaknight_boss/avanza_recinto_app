import React, { Component } from 'react'
import { View,
    ScrollView,
    Image, Text} from 'react-native'
import styles from './style'
import { DrawerItems } from 'react-navigation-drawer'
import AsyncStorage from '@react-native-community/async-storage'
import strings  from '../../asserts/strings'

export default class CustomNavigationDrawer extends Component {

    state = {
        name: '',
        govtId: '',
        recintoId: '',
        recintoName: '',
    }

    componentWillReceiveProps = (props) => {
        if(props.navigation.state.isDrawerOpen){
             AsyncStorage.getItem('profile').then((value) => {
                var profile = JSON.parse(value)
                this.setState({name: profile.coordinator_name, govtId: profile.coordinator_government_id, recintoId: profile.recinto, recintoName: profile.recinto_name});
            });
        }
    }

    componentDidMount() {
  
    }

  render() {
    return (
        <View style={{ flex: 1 }}>
        <View
          style={styles.container}
        >
          <Image
            source={require("../../asserts/images/account.png")}
            style={styles.margin}
          />
          <View style={styles.margin}>
            <Text>
              {this.state.name}
            </Text>
          </View>
          <View style={styles.margin}>
            <Text>
              <Text>{strings.cedula}</Text> <Text> {this.state.govtId}</Text>
            </Text>
          </View>
          <View style={styles.margin}>
            <Text style={styles.textCenter}>
              {this.state.recintoId} {this.state.recintoName}
            </Text>
          </View>
        </View>
        <ScrollView>
          <DrawerItems {...this.props} />
          
        </ScrollView>
      </View>
    )
  }
}