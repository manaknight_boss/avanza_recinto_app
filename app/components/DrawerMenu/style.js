import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';


var styles = StyleSheet.create({
    container: {
        height: 250,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center"
      },
      margin: {
        marginTop: 5,
        marginBottom: 5
      },
      textCenter: {
        textAlign: 'center'
      }
  })
   
  export default styles